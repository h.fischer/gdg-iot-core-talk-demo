require("dotenv").config();
const fs = require("fs");
const jwt = require("jsonwebtoken");
const mqtt = require("mqtt");
const md5 = require("md5");

const deviceId = process.env.ONBOARDING_DEVICE_ID;
const projectId = process.env.PROJECT_ID;
const registryId = process.env.ONBOARDING_REGISTRY_ID;
const region = process.env.REGION;
const algorithm = process.env.CRYPTO_ALG;
const privateKeyFile = deviceId + "_priv.pem.iot";
const mqttBridgeHostname = "mqtt.googleapis.com";
const mqttBridgePort = 443;

// The mqttClientId is a unique string that identifies this device. For Google
// Cloud IoT Core, it must be in the format below.
const mqttClientId = `projects/${projectId}/locations/${region}/registries/${registryId}/devices/${deviceId}`;

console.log(mqttClientId);

const createJwt = (projectId, privateKeyFile, algorithm) => {
  // Create a JWT to authenticate this device. The device will be disconnected
  // after the token expires, and will have to reconnect with a new token. The
  // audience field should always be set to the GCP project id.
  const token = {
    iat: parseInt(Date.now() / 1000),
    exp: parseInt(Date.now() / 1000) + 20 * 60, // 20 minutes
    aud: projectId
  };
  const privateKey = fs.readFileSync(privateKeyFile);
  return jwt.sign(token, privateKey, { algorithm: algorithm });
};

// With Google Cloud IoT Core, the username field is ignored, however it must be
// non-empty. The password field is used to transmit a JWT to authorize the
// device. The "mqtts" protocol causes the library to connect using SSL, which
// is required for Cloud IoT Core.
const connectionArgs = {
  host: mqttBridgeHostname,
  port: mqttBridgePort,
  clientId: mqttClientId,
  username: "unused",
  password: createJwt(projectId, privateKeyFile, algorithm),
  protocol: "mqtts",
  secureProtocol: "TLSv1_2_method"
};

// Create a client, and connect to the Google MQTT bridge.
const client = mqtt.connect(connectionArgs);

client.on("error", err => {
  console.log(err);
  client.end();
});

// Subscribe to the /devices/{device-id}/config topic to receive config updates.
// Config updates are recommended to use QoS 1 (at least once delivery)
client.subscribe(`/devices/${deviceId}/config`, { qos: 1 });

// Subscribe to the /devices/{device-id}/commands/# topic to receive all
// commands or to the /devices/{device-id}/commands/<subfolder> to just receive
// messages published to a specific commands folder; we recommend you use
// QoS 0 (at most once delivery)
client.subscribe(`/devices/${deviceId}/commands/#`, { qos: 0 });

const messageHandler = (topic, message) => {
  let messageStr = "Message received: ";
  if (topic === `/devices/${deviceId}/config`) {
    messageStr = "Config message received: ";
  } else if (topic.startsWith(`/devices/${deviceId}/commands`)) {
    messageStr = "Command message received: ";
  }

  var payload = {};
  try {
    payload = JSON.parse(new Buffer.from(message, "base64").toString());
  } catch {}
  console.log(payload);

  try {
    if (payload.cmd == "deviceCreated") {
      client.end();
      connectAndSendAckToNewRegistry(payload);
    }
  } catch {}
};

const connectAndSendAckToNewRegistry = async onboardingPayload => {
  var iotprivateKeyFile = onboardingPayload.deviceId + "_priv.pem.iot";
  fs.writeFileSync(iotprivateKeyFile, onboardingPayload.privkey);
  var iotmqttClientId = `projects/${onboardingPayload.projectId}/locations/${onboardingPayload.region}/registries/${onboardingPayload.registryId}/devices/${onboardingPayload.deviceId}`;

  var newConnectionArgs = {
    host: mqttBridgeHostname,
    port: mqttBridgePort,
    clientId: iotmqttClientId,
    username: "unused",
    password: createJwt(
      onboardingPayload.projectId,
      iotprivateKeyFile,
      algorithm
    ),
    protocol: "mqtts",
    secureProtocol: "TLSv1_2_method"
  };

  var newClient = mqtt.connect(newConnectionArgs);

  newClient.subscribe(`/devices/${onboardingPayload.deviceId}/config`, {
    qos: 1
  });
  newClient.subscribe(`/devices/${onboardingPayload.deviceId}/commands/#`, {
    qos: 0
  });

  newClient.on("connect", success => {
    var msg = { cmd: "keyStoredAndOnboarded", iotClientId: iotmqttClientId };
    newClient.publish(
      `/devices/${onboardingPayload.deviceId}/events`,
      JSON.stringify(msg),
      { qos: 1 },
      err => {
        if (err) console.log(err);
      }
    );
  });

  newClient.on(`message`, (topic, message) => {
    // console.log(message);
    var newPayload = {};
    try {
      newPayload = JSON.parse(new Buffer.from(message, "base64").toString());
      console.log(newPayload);
    } catch {}
  });
};

// Listen for new messages until timeout is hit
client.on(`message`, messageHandler);

client.on("connect", success => {
  console.log("connect");
  // console.log(client);
  if (!success) {
    console.log("Client not connected...");
  }
  try {
    sendOnboardingState();
  } catch (err) {
    console.error(err);
    client.end(true);
  }
});

var sendOnboardingState = () => {
  var messageType = `events`;
  var mqttTopic = `/devices/${deviceId}/${messageType}`;
  var payload = {
    status: "onboarding",
    onboardingpin: 1337,
    firmware: "v2.0.7",
    device: deviceId
  };
  client.publish(mqttTopic, JSON.stringify(payload), { qos: 1 }, err => {
    if (err) console.log(err);
  });
};

const timeout = 300;

setTimeout(() => {
  client.removeAllListeners();
  client.end();
  console.log(`${messageCount} message(s) received.`);
}, timeout * 1000);
