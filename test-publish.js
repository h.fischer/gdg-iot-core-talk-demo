// Imports the Google Cloud client library
const { PubSub } = require("@google-cloud/pubsub");
require("dotenv").config();

// Creates a client
const pubsub = new PubSub();

const topicName = process.env.DEMO_TELE_TOPIC;
const data = JSON.stringify({ "GDG-DUS": "Happy New Year 2020" });

// Publishes the message as a string, e.g. "Hello, world!" or JSON.stringify(someObject)
const dataBuffer = Buffer.from(data);
pubsub
  .topic(topicName)
  .publish(dataBuffer)
  .then(ret => {
    console.log(`Message ${ret} published.`);
  })
  .catch(err => {
    console.error(`Message ${err} failed.`);
  });
