require("dotenv").config();
const iot = require("@google-cloud/iot");
const { google } = require("googleapis");
const crypto = require("crypto");

const client = new iot.v1.DeviceManagerClient();
const API_VERSION = "v1";
const DISCOVERY_API = "https://cloudiot.googleapis.com/$discovery/rest";

const getClient = async () => {
  // the getClient method looks for the GCLOUD_PROJECT and GOOGLE_APPLICATION_CREDENTIALS
  // environment variables if serviceAccountJson is not passed in
  const authClient = await google.auth.getClient();

  const discoveryUrl = `${DISCOVERY_API}?version=${API_VERSION}`;

  google.options({
    auth: authClient
  });

  try {
    return google.discoverAPI(discoveryUrl);
  } catch (err) {
    console.error("Error during API discovery.", err);
  }
};

exports.send_iot_device_cmd = async (
  projectId,
  cloudRegion,
  registryId,
  deviceId,
  commandMessage
) => {
  var now = new Date();
  var commandMessageTimeStamp = now.toISOString();

  commandMessage = { commandMessageTimeStamp, ...commandMessage };

  var binaryData = Buffer.from(JSON.stringify(commandMessage)).toString(
    "base64"
  );

  var formattedName = client.devicePath(
    projectId,
    cloudRegion,
    registryId,
    deviceId
  );

  // NOTE: The device must be subscribed to the wildcard subfolder
  // or you should specify a subfolder.
  var request = {
    name: formattedName,
    binaryData: binaryData
    //subfolder: <your-subfolder>
  };

  console.log(request);

  return await client
    .sendCommandToDevice(request)
    .then(ret => {
      console.log("Sent command:", commandMessage);
      return;
    })
    .catch(err => {
      console.error(err);
      return;
    });
};

exports.create_iot_device_with_keypair = async (
  projectId,
  cloudRegion,
  registryId,
  deviceId
) => {
  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
  const registryName = `${parentName}/registries/${registryId}`;

  const API_VERSION = "v1";
  const DISCOVERY_API = "https://cloudiot.googleapis.com/$discovery/rest";

  const { publicKey, privateKey } = crypto.generateKeyPairSync("ec", {
    namedCurve: "P-256",
    publicKeyEncoding: {
      type: "spki",
      format: "pem"
    },
    privateKeyEncoding: {
      type: "pkcs8",
      format: "pem"
    }
  });

  var body = {
    id: deviceId,
    credentials: [
      {
        publicKey: {
          format: "ES256_PEM",
          key: publicKey
        }
      }
    ]
  };

  const request = {
    parent: registryName,
    resource: body
  };

  // Call start
  return await getClient()
    .then(client => {
      return client.projects.locations.registries.devices
        .create(request)
        .then(data => {
          console.log("Created device");
          // console.log(data.data);
          return { pub: publicKey, priv: privateKey };
        })
        .catch(err => {
          console.log("Could not create device: ", deviceId);
          // console.log("Message:", err);
          return;
        });
    })
    .catch(err => {
      console.log("Could not get iot mgmt client:");
      return;
    });
};

exports.update_iot_device_config = async (
  projectId,
  cloudRegion,
  registryId,
  deviceId,
  config
) => {
  const parentName = `projects/${projectId}/locations/${cloudRegion}`;
  const registryName = `${parentName}/registries/${registryId}`;
  const version = 0;

  const binaryData = Buffer.from(JSON.stringify(config)).toString("base64");

  const request = {
    name: `${registryName}/devices/${deviceId}`,
    versionToUpdate: version,
    binaryData: binaryData
  };

  return getClient()
    .then(client => {
      return client.projects.locations.registries.devices
        .modifyCloudToDeviceConfig(request)
        .then(data => {
          console.log(
            "Config send:",
            new Buffer.from(data.data.binaryData, "base64").toString()
          );
          return;
        })
        .catch(err => {
          console.log("Could not update config:", deviceId);
          console.log("Message:", err);
          return;
        });
    })
    .catch(err => {
      console.log("Could not update config:", deviceId);
      console.log("Message:", err);
      return;
    });
};
