require("dotenv").config();
const be_iot_common = require("./be_iot_common.js");

const myArgs = process.argv.slice(2);
const deviceId = myArgs[0];
if (deviceId == undefined) {
  return;
}

const customCFG = myArgs[1];

var now = new Date();
const data = {
  timestamp: now.toISOString(),
  deviceName: deviceId,
  deviceFunction: "Be a good Demo device",
  deviceVersion: "0.0.42",
  customCfg: customCFG
};

be_iot_common.update_iot_device_config(
  process.env.PROJECT_ID,
  process.env.REGION,
  process.env.REGISTRY_ID,
  deviceId,
  data
);
