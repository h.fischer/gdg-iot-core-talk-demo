require("dotenv").config();
const be_iot_common = require("./be_iot_common.js");

const myArgs = process.argv.slice(2);
const deviceId = myArgs[0];
if (deviceId == undefined) {
  return;
}

const customMSG = myArgs[1];

const commandMessage = {
  offTopic: "Happy New Year 2020",
  cmd: "OpenDoor",
  msg: customMSG
};

be_iot_common.send_iot_device_cmd(
  process.env.PROJECT_ID,
  process.env.REGION,
  process.env.REGISTRY_ID,
  deviceId,
  commandMessage
);
