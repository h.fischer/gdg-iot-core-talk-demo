require("dotenv").config();
const { google } = require("googleapis");

const myArgs = process.argv.slice(2);
const deviceId = myArgs[0];
if (deviceId == undefined) {
  return;
}

const API_VERSION = "v1";
const DISCOVERY_API = "https://cloudiot.googleapis.com/$discovery/rest";

const data = {
  deviceName: deviceId,
  deviceFunction: "Be a good Demo device",
  deviceVersion: "0.0.42"
};

const projectId = process.env.PROJECT_ID;
const registryId = process.env.REGISTRY_ID;
const cloudRegion = process.env.REGION;

const parentName = `projects/${projectId}/locations/${cloudRegion}`;
const registryName = `${parentName}/registries/${registryId}`;

const request = {
  name: `${registryName}/devices/${deviceId}`
};

const getClient = async () => {
  // the getClient method looks for the GCLOUD_PROJECT and GOOGLE_APPLICATION_CREDENTIALS
  // environment variables if serviceAccountJson is not passed in
  const authClient = await google.auth.getClient();

  const discoveryUrl = `${DISCOVERY_API}?version=${API_VERSION}`;

  google.options({
    auth: authClient
  });

  try {
    return google.discoverAPI(discoveryUrl);
  } catch (err) {
    console.error("Error during API discovery.", err);
  }
};

getClient()
  .then(client => {
    return client.projects.locations.registries.devices.states
      .list(request)
      .then(data => {
        data.data.deviceStates.forEach(state => {
          console.log("time: ", state.updateTime);
          console.log(
            "state: ",
            new Buffer.from(state.binaryData, "base64").toString()
          );
        });

        return;
      })
      .catch(err => {
        console.log("Could not update config:", deviceId);
        console.log("Message:", err);
        return;
      });
  })
  .catch(err => {
    console.log("Could not update config:", deviceId);
    console.log("Message:", err);
    return;
  });
