// Imports the Google Cloud client library
const { PubSub } = require("@google-cloud/pubsub");
require("dotenv").config();

// Creates a client
const pubsub = new PubSub();

const timeout = 300;

// References an existing subscription
const subscription = pubsub.subscription(process.env.DEMO_TELE_SUBSCRIPT);
const subfoldersubscription = pubsub.subscription(
  process.env.DEMO_SUBFOLDER_SUBSCRIPT
);

// Create an event handler to handle messages
let messageCount = 0;
const messageHandler = message => {
  console.log(`Device ${message.attributes.deviceId}:`);
  console.log(`\tReceived message ${message.id}:`);
  console.log(JSON.parse(message.data));
  console.log(message.attributes);
  messageCount += 1;

  // "Ack" (acknowledge receipt of) the message
  message.ack();
};

// Listen for new messages until timeout is hit
subscription.on(`message`, messageHandler);
subfoldersubscription.on(`message`, messageHandler);

setTimeout(() => {
  subscription.removeAllListeners();
  subfoldersubscription.removeAllListeners();
  console.log(`${messageCount} message(s) received.`);
}, timeout * 1000);
