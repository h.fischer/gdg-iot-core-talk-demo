// Imports the Google Cloud client library
const { PubSub } = require("@google-cloud/pubsub");
const be_iot_common = require("./be_iot_common.js");
var uuid = require("uuid");

require("dotenv").config();

// Creates a client
const pubsub = new PubSub();
const timeout = 300;

const projectId = process.env.PROJECT_ID;
const registryId = process.env.REGISTRY_ID;
const cloudRegion = process.env.REGION;
const deviceId = process.env.ONBOARDING_DEVICE_ID;

// References an existing subscription
const subscription = pubsub.subscription(process.env.ONBOARDING_TELE_SUB);

// Create an event handler to handle messages
let messageCount = 0;
const messageHandler = message => {
  var payload = JSON.parse(message.data);
  if (
    payload.status == "onboarding" &&
    message.attributes.deviceId == process.env.ONBOARDING_DEVICE_ID
  ) {
    console.log("onboarding");

    createRealDeviceAndSendKeys();
  }
  message.ack();
};

const createRealDeviceAndSendKeys = () => {
  var newDeviceId = "DEMO" + uuid.v1();
  created = be_iot_common.create_iot_device_with_keypair(
    process.env.PROJECT_ID,
    process.env.REGION,
    process.env.REGISTRY_ID,
    newDeviceId
  );

  var onboardingMsg = created
    .then(keys => {
      return {
        cmd: "deviceCreated",
        projectId: process.env.PROJECT_ID,
        region: process.env.REGION,
        registryId: process.env.REGISTRY_ID,
        deviceId: newDeviceId,
        privkey: keys.priv
      };
    })
    .catch(err => {
      console.error(err);
    });

  var iotDevice = onboardingMsg
    .then(msg => {
      return be_iot_common.send_iot_device_cmd(
        process.env.PROJECT_ID,
        process.env.REGION,
        process.env.ONBOARDING_REGISTRY_ID,
        process.env.ONBOARDING_DEVICE_ID,
        msg
      );
    })
    .catch(err => {
      console.error(err);
    });

  var now = new Date();
  var dummyConfig = {
    timestamp: now.toISOString(),
    deviceName: newDeviceId,
    deviceFunction: "Be a good new Demo device",
    deviceVersion: "0.5.4.2",
    customCfg: "onboardedDevice"
  };

  iotDevice.then(ret => {
    return be_iot_common.update_iot_device_config(
      process.env.PROJECT_ID,
      process.env.REGION,
      process.env.REGISTRY_ID,
      newDeviceId,
      dummyConfig
    );
  });
};

// Listen for new messages until timeout is hit
subscription.on(`message`, messageHandler);

setTimeout(() => {
  subscription.removeAllListeners();
  console.log(`${messageCount} message(s) received.`);
}, timeout * 1000);
