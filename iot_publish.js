require("dotenv").config();
const fs = require("fs");
const jwt = require("jsonwebtoken");
const mqtt = require("mqtt");
const md5 = require("md5");

const myArgs = process.argv.slice(2);
const deviceId = myArgs[0];
if (deviceId == undefined) {
  return;
}

const projectId = process.env.PROJECT_ID;
const registryId = process.env.REGISTRY_ID;
const region = process.env.REGION;
const algorithm = process.env.CRYPTO_ALG;
const privateKeyFile = deviceId + "_priv.pem.iot";
const mqttBridgeHostname = "mqtt.googleapis.com";
const mqttBridgePort = 443;

// The mqttClientId is a unique string that identifies this device. For Google
// Cloud IoT Core, it must be in the format below.
const mqttClientId = `projects/${projectId}/locations/${region}/registries/${registryId}/devices/${deviceId}`;

console.log(mqttClientId);

const createJwt = (projectId, privateKeyFile, algorithm) => {
  // Create a JWT to authenticate this device. The device will be disconnected
  // after the token expires, and will have to reconnect with a new token. The
  // audience field should always be set to the GCP project id.
  const token = {
    iat: parseInt(Date.now() / 1000),
    exp: parseInt(Date.now() / 1000) + 20 * 60, // 20 minutes
    aud: projectId
  };
  const privateKey = fs.readFileSync(privateKeyFile);
  return jwt.sign(token, privateKey, { algorithm: algorithm });
};

// With Google Cloud IoT Core, the username field is ignored, however it must be
// non-empty. The password field is used to transmit a JWT to authorize the
// device. The "mqtts" protocol causes the library to connect using SSL, which
// is required for Cloud IoT Core.
const connectionArgs = {
  host: mqttBridgeHostname,
  port: mqttBridgePort,
  clientId: mqttClientId,
  username: "unused",
  password: createJwt(projectId, privateKeyFile, algorithm),
  protocol: "mqtts",
  secureProtocol: "TLSv1_2_method"
};

// Create a client, and connect to the Google MQTT bridge.
const iatTime = parseInt(Date.now() / 1000);
const client = mqtt.connect(connectionArgs);

// Subscribe to the /devices/{device-id}/config topic to receive config updates.
// Config updates are recommended to use QoS 1 (at least once delivery)
client.subscribe(`/devices/${deviceId}/config`, { qos: 1 });

// Subscribe to the /devices/{device-id}/commands/# topic to receive all
// commands or to the /devices/{device-id}/commands/<subfolder> to just receive
// messages published to a specific commands folder; we recommend you use
// QoS 0 (at most once delivery)
client.subscribe(`/devices/${deviceId}/commands/#`, { qos: 1 });

client.on("error", err => {
  console.log(err);
  client.end();
});

client.on("connect", success => {
  console.log("connect");
  if (!success) {
    console.log("Client not connected...");
  }
  try {
    demoPublish();
  } catch (err) {
    console.error(err);
    client.end(true);
  }

  client.end();
});

var demoPublish = () => {
  // The MQTT topic that this device will publish data to. The MQTT topic name is
  // required to be in the format below. The topic name must end in 'state' to
  // publish state and 'events' to publish telemetry. Note that this is not the
  // same as the device registry's Cloud Pub/Sub topic.
  var messageType = `events`;
  var mqttTopic = `/devices/${deviceId}/${messageType}`;
  var now = new Date();
  var payload = {
    devicetimestamp: now.toISOString(),
    temperature: (18 + Math.random() * 10).toFixed(2),
    lastEvent: "doorOpened",
    "GDG-DUS": "Happy New Year 2020"
  };

  var checksum = md5(payload);
  payload = { ...payload, checksum };

  client.publish(mqttTopic, JSON.stringify(payload), { qos: 1 }, err => {
    if (err) console.log(err);
  });

  mqttTopic = `/devices/${deviceId}/${messageType}/demo-subfolder`;
  now = new Date();
  payload = {
    devicetimestamp: now.toISOString(),
    otherMetricFloat: (100 + Math.random() * 100).toFixed(2),
    details: "subfolder-details"
  };

  var checksum = md5(payload);
  payload = { ...payload, checksum };

  client.publish(mqttTopic, JSON.stringify(payload), { qos: 1 }, err => {
    if (err) console.log(err);
  });

  messageType = `state`;
  mqttTopic = `/devices/${deviceId}/${messageType}`;
  payload = {
    status: "normal operation",
    firmware: "v0.0.41",
    device: deviceId
  };
  client.publish(mqttTopic, JSON.stringify(payload), { qos: 1 }, err => {
    if (err) console.log(err);
  });
};
