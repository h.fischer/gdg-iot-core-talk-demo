include .env
export

create_demo_pubsub:
	gcloud pubsub topics create $(DEMO_TELE_TOPIC)
	gcloud pubsub subscriptions create --topic $(DEMO_TELE_TOPIC) $(DEMO_TELE_SUBSCRIPT)
	gcloud pubsub topics create $(DEMO_STATE_TOPIC)
	gcloud pubsub subscriptions create --topic $(DEMO_STATE_TOPIC) $(DEMO_STATE_SUBSCRIPT)
	gcloud pubsub topics create $(DEMO_SUBFOLDER_TOPIC)
	gcloud pubsub subscriptions create --topic $(DEMO_SUBFOLDER_TOPIC) $(DEMO_SUBFOLDER_SUBSCRIPT)

check_demo_pubsub:
	gcloud pubsub topics describe $(DEMO_TELE_TOPIC)
	gcloud pubsub subscriptions describe $(DEMO_TELE_SUBSCRIPT)
	gcloud pubsub topics describe $(DEMO_STATE_TOPIC)
	gcloud pubsub subscriptions describe $(DEMO_STATE_SUBSCRIPT)
	gcloud pubsub topics describe $(DEMO_SUBFOLDER_TOPIC)
	gcloud pubsub subscriptions describe $(DEMO_SUBFOLDER_SUBSCRIPT)

delete_demo_pubsub:
	gcloud pubsub topics delete $(DEMO_TELE_TOPIC)
	gcloud pubsub subscriptions delete $(DEMO_TELE_SUBSCRIPT)
	gcloud pubsub topics delete $(DEMO_STATE_TOPIC)
	gcloud pubsub subscriptions delete $(DEMO_STATE_SUBSCRIPT)
	gcloud pubsub topics delete $(DEMO_SUBFOLDER_TOPIC)
	gcloud pubsub subscriptions delete $(DEMO_SUBFOLDER_SUBSCRIPT)

create_demo_registry:
	gcloud iot registries create $(REGISTRY_ID) \
		--project=$(PROJECT_ID) \
		--region=$(REGION) \
		--enable-mqtt-config \
		--enable-http-config \
		--state-pubsub-topic=$(DEMO_STATE_TOPIC) \
		--event-notification-config=topic=$(DEMO_SUBFOLDER_TOPIC),subfolder=$(DEMO_SUBFOLDER) \
		--event-notification-config=topic=$(DEMO_TELE_TOPIC)
		
delete_demo_registry:
	gcloud iot registries delete $(REGISTRY_ID) \
		--project=$(PROJECT_ID) \
		--region=$(REGION) \
		--quiet

delete_demo_device:
	gcloud iot devices delete $(DEVICE_ID) \
		--project=$(PROJECT_ID) \
		--region=$(REGION) \
		--registry=$(REGISTRY_ID) \
		--quiet

# Onboarding

create_onboarding_pubsub:
	gcloud pubsub topics create $(ONBOARDING_TELE_TOPIC)
	gcloud pubsub subscriptions create --topic $(ONBOARDING_TELE_TOPIC) $(ONBOARDING_TELE_SUB)

delete_onboarding_pubsub:
	gcloud pubsub topics delete $(ONBOARDING_TELE_TOPIC)
	gcloud pubsub subscriptions delete $(ONBOARDING_TELE_SUB)

create_onboarding_registry:
	gcloud iot registries create $(ONBOARDING_REGISTRY_ID) \
		--project=$(PROJECT_ID) \
		--region=$(REGION) \
		--enable-mqtt-config \
		--event-notification-config=topic=$(ONBOARDING_TELE_TOPIC)

delete_onboarding_registry:
	gcloud iot registries delete $(ONBOARDING_REGISTRY_ID) \
		--project=$(PROJECT_ID) \
		--region=$(REGION) \
		--quiet

create_onboarding_device:
	node be_onboarding_device.js

delete_onboarding_device:
	gcloud iot devices delete $(ONBOARDING_DEVICE_ID) \
		--project=$(PROJECT_ID) \
		--region=$(REGION) \
		--registry=$(ONBOARDING_REGISTRY_ID) \
		--quiet