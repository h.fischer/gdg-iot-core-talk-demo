# GCP Iot Core demo code in javascript / makefile

## 1. Setup your registry
```bash
make create_demo_pubsub
make create_demo_registry
```

this script represent some kind of cloud functions / services running in the google cloud environment.
you can run them with local node
s
## 2. Create device (s)
```bash 
node be_create_device.js <DeviceName>
node be_create_device.js <DeviceName2>
```

## 3. Connect device to the IoT
Either call a publish or a subscribe to the IoT Core from the js IoT device client.
```bash
node iot_publish.js <DeviceName>
node iot_subscribe.js <DeviceName>
```

## 4. update config / send command / get state
```bash
node be_update_config.js <DeviceName>
node be_send_cmd.js <DeviceName>
node be_get_state.js <DeviceName>
```

## Common
Helper script: be_iot_common.js
you don't have to do anything with it

# Onboarding

## Create onboarding IoT registry and pubsub topics
```bash
make create_onboarding_pubsub
make create_onboarding_registry
make create_onboarding_device
```
## Run the onboarding service in the cloud as local js script
```bash
# 1. instances
node be_onboarding_sub.js
```
## Run the client that wants to onboard
```bash
# 2. Instance
node iot_onboarding.js
```

Check the root folder, you should have a new key file there. The device name ist the keyfilename until the suffix ```<newLongDeviceName>_priv.pem.iot```

## Test the new device and call
```bash
node iot_subscribe.js <newLongDeviceName>
node iot_publish.js <newLongDeviceName>
```