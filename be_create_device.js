require("dotenv").config();
const fs = require("fs");
const be_iot_common = require("./be_iot_common.js");

const myArgs = process.argv.slice(2);
const deviceId = myArgs[0];
if (deviceId == undefined) {
  return;
}

be_iot_common
  .create_iot_device_with_keypair(
    process.env.PROJECT_ID,
    process.env.REGION,
    process.env.REGISTRY_ID,
    deviceId
  )
  .then(keys => {
    fs.writeFileSync(deviceId + "_priv.pem.iot", keys.priv);
    fs.writeFileSync(deviceId + "_pub.pem", keys.pub);
  })
  .catch(err => {
    console.log(err);
  });
