require("dotenv").config();
const fs = require("fs");
const be_iot_common = require("./be_iot_common.js");

created = be_iot_common.create_iot_device_with_keypair(
  process.env.PROJECT_ID,
  process.env.REGION,
  process.env.ONBOARDING_REGISTRY_ID,
  process.env.ONBOARDING_DEVICE_ID
);

privkey = created.then(keys => {
  fs.writeFileSync(process.env.ONBOARDING_DEVICE_ID + "_priv.pem.iot", keys.priv);
  fs.writeFileSync(process.env.ONBOARDING_DEVICE_ID + "_pub.pem", keys.pub);
});
